{
  "name": "gitlab-workflow",
  "displayName": "GitLab Workflow",
  "description": "Official GitLab-maintained extension for Visual Studio Code.",
  "version": "3.67.0",
  "publisher": "GitLab",
  "license": "MIT",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/gitlab-org/gitlab-vscode-extension"
  },
  "engines": {
    "vscode": "^1.68.0"
  },
  "categories": [
    "Other"
  ],
  "keywords": [
    "git",
    "gitlab",
    "merge request",
    "pipeline",
    "ci cd"
  ],
  "activationEvents": [
    "onStartupFinished"
  ],
  "bugs": {
    "url": "https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues",
    "email": "incoming+gitlab-org-gitlab-vscode-extension-5261717-issue-@incoming.gitlab.com"
  },
  "galleryBanner": {
    "color": "#171321",
    "theme": "dark"
  },
  "contributes": {
    "configuration": {
      "title": "GitLab Workflow (GitLab VSCode Extension)",
      "properties": {
        "gitlab.debug": {
          "type": "boolean",
          "default": false,
          "description": "Turning on debug mode turns on better stack trace resolution (source maps) and shows more detailed logs. Restart the extension after enabling this option."
        },
        "gitlab.aiAssistedCodeSuggestions.enabled": {
          "description": "Enable code completion (Beta)",
          "type": "boolean",
          "default": false
        }
      }
    },
    "icons": {
      "gitlab-code-suggestions-loading": {
        "description": "GitLab Code Suggestions Loading",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA01"
        }
      },
      "gitlab-code-suggestions-enabled": {
        "description": "GitLab Code Suggestions Enabled",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA02"
        }
      },
      "gitlab-code-suggestions-disabled": {
        "description": "GitLab Code Suggestions Disabled",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA03"
        }
      },
      "gitlab-code-suggestions-error": {
        "description": "GitLab Code Suggestions Error",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA04"
        }
      }
    }
  },
  "scripts": {
    "watch:desktop": "node scripts/watch_desktop.mjs",
    "build:desktop": "node scripts/build_desktop.mjs",
    "build:browser": "node scripts/build_browser.mjs",
    "live:browser": "npm run build:browser && vscode-test-web --browserType=chromium --extensionDevelopmentPath=./dist-browser",
    "package": "node scripts/package.mjs",
    "publish": "vsce publish",
    "clean": "node scripts/clean.mjs",
    "lint": "eslint --ext .js --ext .ts --ext .mjs . && prettier --check '**/*.{js,ts,mjs,vue,json,md}' && npm run --prefix webviews/issuable lint",
    "test": "npm run test:unit && npm run test:integration",
    "test:unit": "jest",
    "prepare:test:integration": "npm run build:desktop && cp -R node_modules dist-desktop/ && node scripts/create_test_workspace.mjs",
    "test:integration": "npm run prepare:test:integration && node ./dist-desktop/test/runTest.js",
    "prettier-package-json": "prettier --write package.json",
    "autofix": "npm run clean && eslint --fix . && prettier --write '**/*.{js,ts,mjs,vue,json,md}' && cd webviews/issuable && npm run autofix",
    "update-ci-variables": "node ./scripts/update_ci_variables.js",
    "create-test-workspace": "npm run build:desktop && node ./scripts/create_workspace_for_test_debugging.js",
    "version": "conventional-changelog -p angular -i CHANGELOG.md -s && git add CHANGELOG.md",
    "postinstall": "npm install --prefix webviews/issuable && npm run prettier-package-json"
  },
  "devDependencies": {
    "@jest/globals": "^29.5.0",
    "@types/jest": "^29.5.2",
    "@types/lodash": "^4.14.195",
    "@types/node": "^13.13.52",
    "@types/request-promise": "^4.1.48",
    "@types/semver": "^7.3.8",
    "@types/sinon": "^10.0.15",
    "@types/source-map-support": "^0.5.4",
    "@types/temp": "^0.9.0",
    "@types/vscode": "^1.68.0",
    "@typescript-eslint/eslint-plugin": "^5.60.0",
    "@typescript-eslint/parser": "^5.60.0",
    "@vscode/test-web": "^0.0.44",
    "conventional-changelog-cli": "^3.0.0",
    "esbuild": "^0.18.0",
    "eslint": "^8.43.0",
    "eslint-config-airbnb-base": "^15.0.0",
    "eslint-config-prettier": "^8.8.0",
    "eslint-plugin-import": "^2.27.5",
    "execa": "^7.1.1",
    "fs-extra": "^11.1.1",
    "jest": "^29.5.0",
    "jest-junit": "^16.0.0",
    "mocha": "^10.2.0",
    "mocha-junit-reporter": "^2.2.0",
    "msw": "^1.2.2",
    "prettier": "^2.8.8",
    "simple-git": "^3.0.0",
    "sinon": "^15.1.2",
    "ts-jest": "^29.1.0",
    "typescript": "^5.0.0",
    "vsce": "^2.0.0",
    "vscode-test": "^1.6.1",
    "webfont": "^11.2.26"
  },
  "dependencies": {
    "cross-fetch": "^3.1.6",
    "dayjs": "^1.10.7",
    "graphql": "^16.6.0",
    "graphql-request": "^6.1.0",
    "https-proxy-agent": "^5.0.1",
    "lodash": "^4.17.21",
    "semver": "^7.3.5",
    "source-map-support": "^0.5.20",
    "temp": "^0.9.4",
    "url": "^0.11.0"
  }
}
